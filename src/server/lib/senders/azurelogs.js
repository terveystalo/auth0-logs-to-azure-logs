const async = require('async');
const useragent = require('useragent');
const request = require('request');
const crypto = require('crypto');

const loggingTools = require('auth0-log-extension-tools');
const config = require('../config');
const logger = require('../logger');

module.exports = () => {
  var getClient = function getClient(workspaceId, workspaceKey, namespace, apiVersion) {
    apiVersion = apiVersion || '2016-04-01';
    var url = "https://" + workspaceId + ".ods.opinsights.azure.com/api/logs?api-version=" + apiVersion;
    var key = new Buffer(workspaceKey, "base64");
    var UTCstring = new Date().toUTCString();
    var hash = function hash(method, contentLength, contentType, date, resource) {
      /* Create the hash for the request */
      var stringtoHash = method + "\n" + contentLength + "\n" + contentType + "\nx-ms-date:" + date + "\n" + resource;
      var stringHash = crypto.createHmac('sha256', key).update(stringtoHash).digest('base64');
      return "SharedKey " + workspaceId + ":" + stringHash;
    };
    return {
      pushRecord: function pushRecord(log) {
        var payload = JSON.stringify(log);
        return new Promise(function (resolve, reject) {
          var signature = hash("POST", payload.length, "application/json", UTCstring, "/api/logs");
          var options = {
            url: url,
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              'Log-Type': namespace,
              'x-ms-date': UTCstring,
              'Authorization': signature,
              'time-generated-field': 'date'
            },
            body: payload
          };
          request(options, function (error, response, body) {
            if (!error && (response.statusCode == 200 || response.statusCode == 202)) {
              resolve();
            } else {
              reject();
            }
          });
        });
      }
    };
  };

  const client = getClient(config('LOGANALYTICS_WORKSPACEID'), config('LOGANALYTICS_WORKSPACEKEY'), config('LOGANALYTICS_NAMESPACE'), config('LOGANALYTICS_APIVERSION'));

  const remapLogs = (record) => {
    record.type_code = record.type;
    record.type = loggingTools.logTypes.get(record.type);

    // Auth0-128 - In Azure logs restrict the entries of log type as sapi
    if(record.type === 'sapi' && (record.description !== undefined && record.description.includes('user'))){
      delete record.details.request;
      delete record.details.response;
    }

    // if user_id contains Telia, remove it
    if (record.user_id !== undefined && record.user_id.includes('Telia')) {
      delete record.user_id;
    }

    // remove user_name from the log entry.
    if (record.user_name !== undefined) {
      delete record.user_name;
    }

    // remove details.prompts from the log entry.
    if (record.details !== undefined) {
      delete record.details.prompts;
      
      // Auth0-124 - Do not send SAML fields to Azure logs
      if (record.details.body !== undefined) {
        delete record.details.body.SAMLResponse;
        delete record.details.body.RelayState;
        delete record.details.body.locale;
      }
    }

    if (record.user_agent && record.user_agent.length) {
      let agent = useragent.parse(record.user_agent);
      record.os = agent.os.toString();
      record.os_version = agent.os.toVersion();
      record.device = agent.device.toString();
      record.device_version = agent.device.toVersion();
    }
    return record;
  };

  return (logs, callback) => {
    if (!logs || !logs.length) {
      return callback();
    }

    logger.info(`Sending ${logs.length} logs to Azure Log Analytics.`);

    async.eachLimit(logs.map(remapLogs), 5, (log, cb) => {
      client.pushRecord(log)
        .then(cb)
        .catch(err => handleError(err));
    }, (err) => {
      if (err) {
        return callback(err);
      }
    });

    logger.info('Upload complete.');
    return callback();
  };
  function handleError(err) {
    console.log(JSON.stringify(err));
  }
};
