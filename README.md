# Auth0 - Logs to Azure Log Analytics

## This extension is build using auth0-logs-to-provider repo. 
The monorepo can be found here: [auth0-logs-to-provider](https://github.com/auth0-extensions/auth0-logs-to-provider)

## How to build the extension
Clone the Monorepo of auth0. 
```sh
git clone https://github.com/auth0-extensions/auth0-logs-to-provider
```
Copy the files from src directory to the cloned directory. 

Execute the following commands to get the build.js file. 

```sh
npm install
node build.js --p azurelogs
```

This will create a file in dist/azurelogs directory. Copy the file to build directory in the current project (auth0-logs-to-azure-logs), and rename the file to bundle.js

## How to install the extension in Auth0

1. Login to Auth0 console. 
1. Go to extensions. 
1. Click on Create Extension
1. In the pop-up, Enter Github Url as <Repo public URL>/raw/HEAD/webtask.json
1. Enter LOGANALYTICS_WORKSPACEID, LOGANALYTICS_WORKSPACEKEY and LOGANALYTICS_NAMESPACE and click on install. 
